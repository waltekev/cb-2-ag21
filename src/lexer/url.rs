use logos::{Lexer, Logos, Source};
use std::fmt::{Display, Formatter};

/// Tuple struct for link URLs
#[derive(Debug, PartialEq)]
pub struct LinkUrl(String);

/// Implement Display for printing
impl Display for LinkUrl {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Tuple struct for link texts
#[derive(Debug, PartialEq)]
pub struct LinkText(String);

/// Implement Display for printing
impl Display for LinkText {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Token enum for capturing of link URLs and Texts
#[derive(Logos, Debug, PartialEq)]
pub enum URLToken {
    // TODO: Capture link definitions
    #[regex("<a [^>]*href=\"[^>]*>[^<]*</a\\s*>", extract_link_info)]
    Link((LinkUrl, LinkText)),
    
    #[regex("[^<]*", logos::skip)]
    #[regex("<[^>]*>", logos::skip)]
    Ignored,


    // Catch any error
    #[error]
    Error,
}

/// Extracts the URL and text from a string that matched a Link token
fn extract_link_info(lex: &mut Lexer<URLToken>) -> (LinkUrl, LinkText) {
    // TODO: Implement extraction from link definition
    let text = lex.slice();
    let mut hrefIndex: usize = 0;
    let mut hrefIndexEnd: usize = 0;
    let mut hrefIndexFound = false;
    let mut hrefIndexEndFound = false;
    
    let mut linkTextIndex: usize = 0;
    let mut linkTextIndexEnd: usize = 0;
    let mut linkTextIndexFound = false;
    for (i, c) in text.chars().enumerate() {
        if !hrefIndexEndFound {
            if c == 'h' && !hrefIndexFound {
                let href = &text[i..i+6];
                if href == "href=\"" {
                    hrefIndex = i + 6;
                    hrefIndexFound = true;
                }
            }else if hrefIndexFound && i > hrefIndex {
                if c == '\"' {
                    hrefIndexEnd = i;
                    hrefIndexEndFound = true;
                }
            }
        }else {
            if c == '>' && !linkTextIndexFound {
                linkTextIndex = i + 1;
                linkTextIndexFound = true;
            }else if c == '<' && linkTextIndexFound {
                linkTextIndexEnd = i;
                break;
            }
        }
    }
    
    return (LinkUrl(String::from(&text[hrefIndex..hrefIndexEnd])), LinkText(String::from(&text[linkTextIndex..linkTextIndexEnd])));
}
